---
title: "NDSS Model maintenance and calibration transfer "
# subtitle: "What comes next?"
author: 
    - "Jean-Michel Roger"
    - "Valeria Fonseca Díaz"
format: 
    revealjs:
        theme: white
        self-contained: false
        slide-number: false
        width: 1300
        height: 700
		# pdfseparatefragments: true
        # presentation-h1-font-size: 0.1em
        # chalkboard: true
        # navigation-mode: grid
        preview-links: auto
        # parallax-background-image: "./assets/mylogo.png"
        # parallax-background-size: 250px 100px
        title-slide-attributes:
            data-background-image: "./assets/sensorfint_logo.png" 
            data-background-position: "bottom 8% left 5%" 
            data-background-size : "65%"
        logo: ./assets/foot_logo2.png
        # fig-align: center
        # mermaid-format: svg
		pdf-separate-fragments: true
---


# {style="font-size:60%" background-image="./assets/mylogo.png" background-position="bottom 10% left 90%" background-size="40%"}



::: {style="font-size: 2.5em; text-align: left"}

**Calibration transfer and maintenance**

::: 

::: {style="font-size: 2.5em; text-align: left"}

_What comes next?_

::: 


# Towards more efficient calibration


## 1. Emphasis on _efficient_ methods


::: {.r-stack}

::: {.fragment .fade-in-then-out}

- _Research_: Develop methods that require few to none reference values/standard samples

- _Practice_: Access to modern efficient methods

:::


::: {.fragment .fade-in-then-out}

> State of the art


<table style="font-size: 0.7em">
									<thead>
										<tr>
											<th>Methodology</th>
											<th colspan="3">Types of samples</th>
										</tr>
										<tr>
											<th>Category</th>
											<th>Standards</th>
											<th>Target spectra and reference values</th>
											<th>Only target spectral measurements</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td> Linear standardization</td>
											<td> DS, PDS, SST</td>
											<td> </td>
											<td > Spectral mean correction</td>
										</tr>
										<tr>
											<td> Prediction correction</td>
											<td> </td>
											<td> SBC</td>
											<td> </td>
										</tr>
										<tr>
											<td> Concatenation</td>
											<td> Joint PCA</td>
											<td> Joint PLS</td>
											<td> </td>
										</tr>
										<tr>
											<td> Orthogonalization</td>
											<td> TOP</td>
											<td> DOP</td>
											<td> uDOP</td>
										</tr>
										<tr>
											<td> Variable selection</td>
											<td> CAWS</td>
											<td> </td>
											<td > di-CovSel</td>
										</tr>
										<tr>
											<td> Domain invariant</td>
											<td> </td>
											<td> Supervised di-PLS</td>
											<td > Unsupervised di-PLS</td>
										</tr>


									</tbody>
								</table>



:::

:::

## 2. Transfer to multiple domains 


::: {.r-stack}
::: {.fragment .fade-in-then-out}

_Research_: Transfer to one target domain should be a specific case

:::

::: {.fragment .fade-in-then-out}

> _Ex_: Unsupervised orthogonalization on 2 domains

![](./assets/figures/2_udop.png){width="100%" fig-align="center"}


:::

::: {.fragment .fade-in-then-out}

> _Ex_: How to extend it to multiple domains?

::: {style="font-size: 0.6em"}

+----------+------------+----------+--------+------+
| Domain   | Type       | Cultivar | Season | n    |   
+==========+============+==========+========+======+
| Source   | Hard Green | Caly     | 1      | 1277 |  
+----------+------------+----------+--------+------+  
| Target 1 | Hard Green | KP       | 1      | 1059 |
+----------+------------+----------+--------+------+    
| Target 2 | Hard Green | HG       | 1      | 293  |
+----------+------------+----------+--------+------+     
| Target 3 | Hard Green | KP       | 2      | 340  |
+----------+------------+----------+--------+------+    
| Target 4 | Hard Green | HG       | 2      | 88   |
+----------+------------+----------+--------+------+     
| Target 5 | Hard Green | R2E2     | 2      | 276  |
+----------+------------+----------+--------+------+    
| Target 6 | Hard Green | KP       | 3      | 216  |
+----------+------------+----------+--------+------+    
| Target 7 | Hard Green | HG       | 3      | 80   |
+----------+------------+----------+--------+------+   
| Target 8 | Hard Green | R2E2     | 3      | 156  |
+----------+------------+----------+--------+------+    
| Target 9 | Ripen      | KP       | 3      | 78   |
+----------+------------+----------+--------+------+    


: {tbl-colwidths="[10,50,15,15,10]"}

:::
:::
:::





## 3. Quantify degree of transferrability

::: {.r-stack}
::: {.fragment .fade-in-then-out}

_Research_: Define metrics to quantify how related the domains are

:::

::: {.fragment .fade-in-then-out}

For a couple of instruments _s_ and _t_:

 - $$ X_s = T_sV_s' + E_s $$

- $$ X_t = T_tV_t' + E_t $$

- Degree of linearity = $|V_s'V_t|$



:::

::: {.fragment .fade-in-then-out}

![](./assets/figures/2_linearity_couples.png){width="60%" fig-align="center"}

:::
:::

# Towards more efficient maintenance


## 1. Monitoring a calibration model

::: {.r-stack}

::: {.fragment .fade-in-then-out}

_Research_: Keep focus on model degradation framework

:::


::: {.fragment .fade-in-then-out}



![](./assets/figures/3_maintenance_1.png){width="100%" fig-align="center"}

:::

::: {.fragment .fade-in-then-out}


![](./assets/figures/3_maintenance_2.png){width="100%" fig-align="center"}

:::


::: {.fragment .fade-in-then-out}

> Causes

![](./assets/figures/3_maintenance_spectra_y.png){width="100%" fig-align="center"}

:::

::: {.fragment .fade-in-then-out}

> Types: Regression

![](./assets/figures/3_regression_maintenance.png){width="100%" fig-align="center"}

:::

::: {.fragment .fade-in}


> Types: Classification

![](./assets/figures/3_classification_maintenance.png){width="100%" fig-align="center"}


::: {.fragment .fade-in}

Possibility to find a mapping between interferents and type of degradation

:::
:::

:::



## 2. Quantifying degradation with minimum information

::: {.r-stack}


::: {.fragment .fade-in-then-out}

_Research_: Define specific metrics with respect to the previous framework

:::

::: {.fragment .fade-in-then-out}

> Cause $P(X)$: using PCA. No need for reference values

![](./assets/figures/3_maintenance_t2q2.png){width="80%" fig-align="center"}

:::


::: {.fragment .fade-in-then-out}

> Type Bias: re-estimate the intercept of the model only with new $X$.

- Current model: $\hat{y} = x\hat{B} + \hat{\beta}$ . ($\hat{\beta} = \bar{y} - \bar{x}\hat{B}$)

- Modify model: Replace $\hat{\beta}$ by using mean of $X_t$.

- Predict initial test samples with modified model

$$|bias|_t = \left\vert \frac{1}{n}\sum_{i=1}^n y_i - \hat{y}_{i_t} \right\vert $$

:::

::: {.fragment .fade-in-then-out}



![](./assets/figures/3_maintenance_bias.png){width="80%" fig-align="center"}



:::

::: {.fragment .fade-in}

> How to quantify _without_ new reference values?

![](./assets/figures/3_maintenance_3.png){width="80%" fig-align="center"}


::: {.fragment .fade-in}

Even with new reference values, non-linearity?

:::

:::

:::



## 3. Model update



::: {.r-stack}
::: {.fragment .fade-in-then-out}

_Practice_: Follow proposed framework for model update

:::

::: {.fragment .fade-in-then-out}


![](./assets/figures/3_maintenance_update_scheme.png){width="80%" fig-align="center"}

:::




:::



## 4. When is time for full recalibration?


::: {.r-stack}
::: {.fragment .fade-in-then-out}

_Research_: Distribution and thresholds for the monitoring metrics

:::
::: {.fragment .fade-in-then-out}

> Has the model completely broken down yet?

![](./assets/figures/3_maintenance_fullrecal.png){width="100%" fig-align="center"}

:::
:::


## Conclusions {background="#43464B" transition="convex"}

<br>

::: {.fragment .fade-left}
- Calibration transfer and maintenance is a widely studied issue 
:::

::: {.fragment .fade-left}
- A lot of methods, for a lot of situations 
:::

::: {.fragment .fade-left}
- Methods from machine learning could be adapted to chemometrics 
:::

::: {.fragment .fade-left}
- There is still some space for innovations 
:::

::: {.fragment .fade-left}
- The Grail is not still found...

:::

## Chemometrics in practice {background="#43464B" transition="convex"}



::: {.fragment .fade-left}
- Access to raw data
    - Without raw data, interferents and efficient model adaptation is limited
:::

::: {.fragment .fade-left}
- Access to new methods for any chemometrics modelling task
    - Need to increase availability of open source software
:::

::: {.fragment .fade-left}
- Deployment of models after building, transferring, and updating
    - Models need to be saved and used as one entity
:::






# Thank you!


::: {style="text-align:center;font-size: 0.7em"}


Prof. dr. Marina Cocchi (Unimore, Italy)  <br>
Prof. dr. ir. Ben Aernouts (Livestock Techology, KU Leuven) <br>
Els Bobelyn (VCBT, KU Leuven)  <br>

<br>
<br>

SensorFINT

:::



<!-- # Memes -->





